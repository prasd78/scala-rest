scalaVersion := "2.11.7" // Also supports 2.10.x
sbtPlugin := true
lazy val http4sVersion = "0.14.1"

// Only necessary for SNAPSHOT releases
resolvers += Resolver.sonatypeRepo("snapshots")

resolvers += "spray repo" at "http://repo.spray.io/"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion

)

scalacOptions := Seq("-deprecation", "-unchecked", "-feature")

