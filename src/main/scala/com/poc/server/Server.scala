package com.poc.server

import org.http4s.server.{Server, ServerApp}
import org.http4s.server.blaze._
import scalaz.concurrent.Task
import org.http4s.server.syntax._
import com.poc.services.HalloService.hello
import com.poc.services.GoodbyeService.goodbye

object Server extends ServerApp {
  val services = hello orElse goodbye

  override def server(args: List[String]): Task[Server] = {
    BlazeBuilder
      .bindHttp(80, "localhost")
      .mountService(services, "/api")
      .start

  }
}
