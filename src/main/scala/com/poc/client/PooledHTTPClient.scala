package com.poc.client

import org.http4s.client.blaze.PooledHttp1Client

import scalaz.concurrent.Task

/**
  * Created by achakrab on 10/11/16.
  */
class PooledHTTPClient {

  val client = PooledHttp1Client()
  val helloAnirban = client.expect[String]("http://localhost:8080/hello/Anirban")

  def hello(name: String): Task[String] = {
    //val target = uri("http://localhost:8080/hello/") / name
    client.expect[String](s"http://localhost:8080/hello/ $name")
  }

  val people = Vector("Michael", "Jessica", "Ashley", "Christopher")
  val greetingList = Task.gatherUnordered(people.map(hello))
}
