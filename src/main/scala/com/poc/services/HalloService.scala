package com.poc.services

import org.http4s._
import org.http4s.dsl._

object HalloService {
  val hello = HttpService {
    case GET -> Root / "hello" / name =>
      Ok(s"Hello, $name.")
  }
}
