package com.poc.services

import org.http4s._
import org.http4s.dsl._

object GoodbyeService {
  val goodbye = HttpService {
    case GET -> Root / "goodbye" / name =>
      Ok(s"Goodbye, $name.")
  }
}
